//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

//create student one
let studentOneName = "John";
let studentOneEmail = "john@mail.com";
let studentOneGrades = [89, 84, 78, 88];

//create student two
let studentTwoName = "Joe";
let studentTwoEmail = "joe@mail.com";
let studentTwoGrades = [78, 82, 79, 85];

//create student three
let studentThreeName = "Jane";
let studentThreeEmail = "jane@mail.com";
let studentThreeGrades = [87, 89, 91, 93];

//create student four
let studentFourName = "Jessie";
let studentFourEmail = "jessie@mail.com";
let studentFourGrades = [91, 89, 92, 93];

//actions that students may perform will be lumped together
function login(email) {
  console.log(`${email} has logged in`);
}

function logout(email) {
  console.log(`${email} has logged out`);
}

function listGrades(grades) {
  grades.forEach((grade) => {
    console.log(grade);
  });
}

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects

// Use an object literal
// ENCAPSULATION
let studentOne = {
  name: "John",
  email: "john@gmail.com",
  grades: [89, 84, 78, 88],
  login() {
    console.log(`${this.email} has logged in`);
  },
  logout() {
    console.log(`${this.email} has logged out`);
  },
  listGrades() {
    console.log(`${this.name}'s quarterly grade averages are ${this.grades}`);
  },
  computeQuarterlyAverage() {
    const sum = this.grades.reduce((acc, curr) => acc + curr, 0);
    const avg = sum / this.grades.length;
    return avg;
  },
  // mini exercise 2
  willPass() {
    if (this.computeQuarterlyAverage() >= 85) {
      return true;
    } else return false;
  },
  willPassWithHonors() {
    return this.willPass() && this.computeQuarterlyAverage() >= 90
      ? true
      : false;
  },
};

console.log(`student one's name is ${studentOne.name}`);
console.log(`student one's email is ${studentOne.email}`);
console.log(`student one's grades are ${studentOne.grades}`);

console.log(studentOne.computeQuarterlyAverage());

console.log(studentOne.willPass());
console.log(studentOne.willPassWithHonors());

class Student {
  constructor(name, email, grades) {
    this.name = name;
    this.email = email;
    this.grades = grades;
  }
  computeQuarterlyAverage() {
    const sum = this.grades.reduce((acc, curr) => acc + curr, 0);
    const avg = sum / this.grades.length;
    return avg;
  }
  // mini exercise 2
  willPass() {
    if (this.computeQuarterlyAverage() >= 85) {
      return true;
    } else return false;
  }
  willPassWithHonors() {
    return this.willPass() && this.computeQuarterlyAverage() >= 90
      ? true
      : false;
  }
}

let studentTwo = new Student(studentTwoName, studentTwoEmail, studentTwoGrades);
let studentThree = new Student(
  studentThreeName,
  studentThreeEmail,
  studentThreeGrades
);
let studentFour = new Student(
  studentFourName,
  studentFourEmail,
  studentFourGrades
);
console.log("this time for student two");
console.log(`student two's name is ${studentTwo.name}`);
console.log(`student two's email is ${studentTwo.email}`);
console.log(`student two's grades are ${studentTwo.grades}`);

console.log(studentTwo.computeQuarterlyAverage());

console.log(studentTwo.willPass());
console.log(studentTwo.willPassWithHonors());
console.log("this time for student three");
console.log(`student three's name is ${studentThree.name}`);
console.log(`student three's email is ${studentThree.email}`);
console.log(`student three's grades are ${studentThree.grades}`);

console.log(studentThree.computeQuarterlyAverage());

console.log(studentThree.willPass());
console.log(studentThree.willPassWithHonors());

console.log("this time for student four");
console.log(`student Four's name is ${studentFour.name}`);
console.log(`student Four's email is ${studentFour.email}`);
console.log(`student Four's grades are ${studentFour.grades}`);

console.log(studentFour.computeQuarterlyAverage());

console.log(studentFour.willPass());
console.log(studentFour.willPassWithHonors());

let classOf1A = [studentOne, studentTwo, studentThree, studentFour];

console.log(classOf1A);
classOf1A.countHonorStudents = () => {
  let count = 0;
  classOf1A.forEach((student) => {
    // console.log(student.willPassWithHonors());
    if (student.willPassWithHonors()) {
      count += 1;
    }
    console.log(count);
  });
  return count;
};
classOf1A.honorsPercentage = () => {
  return (classOf1A.countHonorStudents() / classOf1A.length) * 100;
};

console.log("class of 1A");
console.log(classOf1A);
console.log("class of 1A with honors");
console.log(classOf1A.countHonorStudents());
console.log("honors percentage");
console.log(classOf1A.honorsPercentage());

//QUIZ
/*
1. spaghetti code
2. let myObject = {...}
3. object oriented programming
4. studentOne.enroll()
5. true. nested
6. key: value
7. true
8. true
9. true
10. true
*/
